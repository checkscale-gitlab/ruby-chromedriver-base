FROM ruby:2.5.0

MAINTAINER Vehiculum mobility solutions GmbH <info@vehiculum.de>

# Update system and install main dependencies
RUN apt-get update && apt-get install -y --no-install-recommends wget xvfb unzip

# Install Google Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update -y
RUN apt-get install -y google-chrome-stable

# Chromedriver Environment variables
ENV CHROMEDRIVER_VERSION 2.39
ENV CHROMEDRIVER_DIR /usr/bin

# Download and install Chromedriver
RUN wget -q --continue -P $CHROMEDRIVER_DIR "http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip"
RUN unzip $CHROMEDRIVER_DIR/chromedriver* -d $CHROMEDRIVER_DIR
RUN rm $CHROMEDRIVER_DIR/chromedriver*.zip
